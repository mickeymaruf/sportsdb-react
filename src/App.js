import './App.css';
import { useState } from 'react';
import { Main } from './Components/Main/Main';
// import Users from './Components/Users/Users';
import Navbar from './Components/Navbar/Navbar';

import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { useEffect } from 'react';
import AOS from "aos";
import "aos/dist/aos.css";

function App() {
  useEffect(() => {
    AOS.init();
    AOS.refresh();
  }, []);

  // it's been here for using the Cart inside navbar
  const [cart, setCart] = useState([]);

  return (
    <div className="App">
      <ToastContainer />
      <Navbar cart={cart} setCart={setCart}></Navbar>
      <Main cart={cart} setCart={setCart}></Main>
    </div>
  );
}

export default App;
