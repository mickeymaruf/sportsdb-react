import React from 'react';
import Cart from '../Cart/Cart';
import Players from '../Players/Players';

const Main = ({cart, setCart}) => {
    return (
        <main className='bg-base-300 grid grid-cols-1 lg:grid-cols-3'>
            <Players cart={cart} setCart={setCart}></Players>
            <div className='hidden lg:block'>
                <Cart cart={cart} setCart={setCart}></Cart>
            </div>
        </main>
    );
};

export {Main, Cart};