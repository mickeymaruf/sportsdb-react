import React from 'react';
import Player from '../Player/Player';
import { useEffect, useState } from 'react';
import Search from '../Search/Search';

const Players = ({cart, setCart}) => {
    const [search, setSearch] = useState("");

    const [players, setPlayers] = useState([]);
    useEffect(()=>{
        fetch(`https://www.thesportsdb.com/api/v1/json/2/searchplayers.php?p=${search}`)
        .then(res => res.json())
        .then(data => setPlayers(data?.player))
    }, [search])

    return (
        <div className='col-span-2 px-10 py-5 pb-10'>
            <Search setSearch={setSearch}></Search>
            {
            players ?
            <div className='grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-2 xl:grid-cols-3 2xl:grid-cols-4 gap-5'>
                {
                    players.map(player => <Player
                        key={player.idPlayer}
                        player={player}
                        cart={cart}
                        setCart={setCart}
                    ></Player>)
                }
            </div>
            :
            <div className='py-5 text-left text-2xl font-medium'>
                <h1>Nothings Found Here!</h1>
            </div>
            }
        </div>
    );
};

export default Players;