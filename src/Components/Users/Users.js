import React, { useState, useEffect } from 'react';
import User from '../User/User';
import './Users.css'

const Users = () => {
    const users = [
        {id: 1, name: "Maruf", age:20, job: "Software eng."},
        {id: 2, name: "Sakib", age:20, job: "Wev dev."},
        {id: 3, name: "Tamim", age:20, job: "Electronics eng."},
        {id: 4, name: "Ninja", age:20, job: "Ninja"},
        {id: 5, name: "Ashik", age:20, job: "Varun Farmacy"},
    ]
    
    const [data, setData] = useState([]);
    
    useEffect(() => {
        setData(users);
    }, [])
    
    const deleteUser = (id) => {
        const rest = data.filter(u => u.id !==id);
        setData([...rest])
    }
    return (
        <div>
            <h1>Delete Users!</h1>
            <div className='users'>
                {
                    data.map(user => <User
                        key={user.id}
                        user={user}
                        deleteUser={deleteUser}
                        ></User>)
                }
            </div>
        </div>
    );
};

export default Users;