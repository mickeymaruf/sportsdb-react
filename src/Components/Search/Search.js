import React from 'react';

const Search = ({setSearch}) => {
    return (
        <div className="form-control w-full max-w-xs mb-5">
            <label className="label">
                <span className="label-text">Search your player here</span>
                <span className="label-text-alt"><div className="kbd">Type</div></span>
            </label>
            <input
                onChange={(e) => setSearch(e.target.value)}
                type="text" placeholder="Search here"
                className="input input-bordered w-full max-w-xs"
            />
        </div>
    );
};

export default Search;