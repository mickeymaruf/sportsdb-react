import React from 'react';
import './User.css'

const User = ({ user, deleteUser }) => {
    return (
        <div className='user'>
            <h3>{user.name}</h3>
            <h3>{user.age}</h3>
            <h2>{user.job}</h2>
            <button onClick={() => deleteUser(user.id)}>Delete</button>
        </div>
    );
};

export default User;