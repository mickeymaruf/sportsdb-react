import React from 'react';
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'

const MySwal = withReactContent(Swal);

const Player = ({player, cart, setCart}) => {
    const {idPlayer, strPlayer, strThumb} = player;
    const handleAddToCart = () => {
        let cartPlayers = [];
        const cartIsExist = localStorage.getItem('players-cart');
        if(cartIsExist){
            cartPlayers = JSON.parse(cartIsExist);
        }
        // 
        const isExist = cart.find(p => p.idPlayer === player.idPlayer);
        if(isExist){
            isExist.quantity++;
            setCart([...cart]);
            cartPlayers.find(player => isExist.idPlayer === player.idPlayer).quantity++;
        } else{
            const info = {idPlayer, strPlayer, strThumb, quantity: 1};
            // 
            setCart([...cart, info]);
            cartPlayers.push(info);
        }
        // 
        localStorage.setItem('players-cart', JSON.stringify(cartPlayers))

        MySwal.fire({title: <strong>Player Added!</strong>, icon: 'success', timer: 900, showConfirmButton: false})
    }

    return (
        <div className="card bg-base-100 shadow-xl" data-aos="zoom-in">
            <figure><img className='w-full' src={
                strThumb || "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSrq4gJP1bprNtOo5nC_6ILk92McwNurNFnRUXk0mPmJgKLvhX3oquI5-lYQkv5LspZL14&usqp=CAU"
            } alt="Shoes" /></figure>
            <div className="card-body p-3 grid">
                <h2 className="card-title">{strPlayer}</h2>
                <div className="card-actions justify-end">
                    <button onClick={handleAddToCart} className="btn btn-primary w-full self-end">Buy Now</button>
                </div>
            </div>
        </div>
    );
};

export default Player;