import React, { useEffect } from 'react';
import { toast } from 'react-toastify';

const Cart = ({ cart, setCart }) => {
    const handleDeletePlayer = (id) => {
        const restCart = cart.filter(player => player.idPlayer !== id);
        setCart(restCart);
        
        // deleting from localStorage too
        localStorage.setItem('players-cart', JSON.stringify(restCart));

        toast.success("Player Removed!", { autoClose: 1500 });
    }

    useEffect(() => {
        const playersCart = JSON.parse(localStorage.getItem('players-cart'));
        if(playersCart){
            setCart(playersCart);
        }
    }, [setCart])

    return (
        <aside className='sidebar shadow-sm lg:bg-base-100 rounded-md sticky self-start top-0 lg:min-h-screen lg:p-5 lg:m-5 w-full lg:w-11/12'>
            <div className='grid gap-y-3'>
                {
                    cart.map(player => <CartItem
                        key={player.idPlayer}
                        player={player}
                        handleDeletePlayer={handleDeletePlayer}
                    ></CartItem>)
                }
            </div>
        </aside>
    );
};

const CartItem = ({ player, handleDeletePlayer }) => {
    const { idPlayer, strPlayer, strThumb, quantity } = player;
    return (
        <table>
            <tbody>
                <tr className='glass flex items-center p-2 rounded-md'>
                    <td><img className='w-16 rounded-xl' src={strThumb || "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSrq4gJP1bprNtOo5nC_6ILk92McwNurNFnRUXk0mPmJgKLvhX3oquI5-lYQkv5LspZL14&usqp=CAU"} alt="" /></td>
                    <td className='text-lg font-medium justify-self-start mr-auto ml-5'>{strPlayer}</td>
                    <td>{quantity}</td>
                    <td><button onClick={() => handleDeletePlayer(idPlayer)} className='bg-red-600 text-white py-2 px-3 ml-14'>Remove</button></td>
                </tr>
            </tbody>
        </table>
    )
}

export default Cart;